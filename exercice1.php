<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
$v = 5;
    /*Effectuer les opérations suivantes sur la variable $variable :
     - multiplier la variable par 2
     - multiplier la variable par elle-même

    Attention: ne pas utiliser echo ou print
    (déjà présent dans l'exercice)*/
?>
<!-- écrire le code après ce commentaire -->
<?php
    $v *= 2;
    $v *= $v;
?>


<!-- écrire le code avant ce commentaire -->
<?php

echo '<p>Resultat: '.$v.'</p>';

?>
</body>
</html>
