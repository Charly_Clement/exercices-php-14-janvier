<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
$v = 7;
    /*Effectuer les opérations suivantes sur la variable $variable :
     - diviser la variable par 2
     - arrondir la variable à l'entier inférieur à l'aide de la fonction
     floor() : https://www.php.net/manual/fr/function.floor.php

    Attention: ne pas utiliser echo ou print
    (déjà présent dans l'exercice)*/
?>
<!-- écrire le code après ce commentaire -->
<?php
    /* $v = $v / 2;
    $v = floor($v);*/

    //OU
    
    $v = floor($v/2);
?>
<!-- écrire le code avant ce commentaire -->
<?php

echo '<p>Resultat: '.$v.'</p>';

?>
</body>
</html>
