<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
$etat = "je suis liiiiibre !";

$de1 = mt_rand(1, 6);
$de2 = mt_rand(1, 6);
$de3 = mt_rand(1, 6);

/*
    Le joueur lance 3 fois un dé à 6 faces, les valeurs sont 
    enregistrées dans les variables $de1, $de2, $de3

    le joueur perd et part en prison dans LES CAS ou :
    - la somme des valeurs des dés est égale à 9
    - le dé 1 est plus grand que 4 et le dé 3 est plus petit que 3

    Attention: ne pas utiliser echo ou print, seulement modifier la
    valeur des variables
*/
?>
<!-- écrire le code après ce commentaire -->
<?php
    $somme = $de1 + $de2 + $de3;
    if ($somme == 9 || $de1 > 4 && $de2 < 3){
        $etat "je suis en prison";
    }
?>


<!-- écrire le code avant ce commentaire -->
<?php

echo '<p>Dé 1: '.$de1.'</p>';
echo '<p>Dé 2: '.$de2.'</p>';
echo '<p>Dé 3: '.$de3.'</p>';
echo '<p>Etat: '.$etat.'</p>';

?>
</body>
</html>
